from asoracle import asoracle
from asoracle import secrets
import pandas as pd
import config
import pprint
from pymongo import MongoClient

# setup
client = MongoClient()
db = client.local
collection = db.regressionResults
edw = asoracle.asoracle(secrets.user, secrets.password, config.conn_str)

net_bkgs = edw.sqltoDF('yff-halcurves.sql') # GAP >= 0 & FLT_DIV_Y between 0 and 3
comp_data = edw.sqltoDF('curr-infare.sql') # limits to between 21 and 90 DTD
selling_fares = edw.sqltoDF(sql='SELECT * FROM CWU.LAXJFK_FARES') # limits to 90 days out

net_bkgs = net_bkgs[net_bkgs.OPER_FLT_NBR == 'AS1404'] # focus on AS1404/VX1404 for now

col = ['FLT_NBR', 'DPTR_DATE', 'ORIG', 'DEST', 'OD', 'DPTR_DATE', 
	'SELLING_BKT', 'SELLING_FARE' 'FLT_GAP_Y', 'FLT_RI_Y', 'FLT_DIV_Y', 'FLT_LAST_WORKED',
	'D0_Y_CAB_CLS_CLSD_CNT']

results = pd.DataFrame(columns=col)

for index, row in net_bkgs.iterrows(): # loop through qualifying flights
	# pull in relevant data
	doc = collection.find({'FLT_NBR':row['OPER_FLT_NBR']},{'COEF': 1})[0]
	coef = doc.get('COEF')
	fare = selling_fares[(selling_fares['DPTR_DATE'] == row['DPTR_DATE']) 
		& (selling_fares['OD'] == row['OD'])].sort_values('FARE_AMOUNT', ascending=False)
	comp = comp_data[(comp_data['DPTR_DATE'] == row['DPTR_DATE']) 
		& (comp_data['OD']==row['OD'])]

	for bucket_i, bucket_r in fare.iterrows(): # loop through selling buckets
		fare_amount = bucket_r['FARE_AMOUNT']
		tst = [(float(y) - float(bucket_r['FARE_AMOUNT']) ) * float(coef.get(x)) for x, y in zip(comp['CAT'], comp['AMT'])]
		if sum(tst) >= float(row['FCST_BKGS']) or bucket_r['BUCKET'] == 'R':
			print(f"The ideal selling bucket for {row['OPER_FLT_NBR']} on {row['DPTR_DATE']} \
				is {bucket_r['BUCKET']} to achieve {row['FCST_BKGS']} bkgs")
			row['SELLING_BKT'], row['SELLING_FARE'] = bucket_r[['BUCKET', 'FARE_AMOUNT']]
			break

	results = results.append(row[col])
	# raise Exception('breakpoint')

print(results.head())
results.to_csv('sample_output.csv')
