from sklearn.linear_model import ElasticNet
from sklearn import metrics
import pandas as pd
import csv
import numpy as np
import itertools
from pymongo import MongoClient

client = MongoClient()
db = client.local
collection = db.regressionResults

df = pd.read_csv('VX1404.csv')
df.drop(columns='AS_PRICE', inplace=True)
print(df.head())
df =df[(df.DTD >= 21) & (df.DTD <= 90)]

print(f'length of dataframe: {len(df)}')
index = set(df.DOW)

col = ['NDOD', 'OD', 'FLT_NBR', 'DOW', 'AP', 'COEF', 'MSE', 'MAE', 'ELASTICNET_SCORE'] # add a snapshot date
output = pd.DataFrame(columns=col)
# output.drop(columns=['OBSERVATION_DT', 'DTD'])

print(set(df.AP))
print(set(df.DOW))

for i in list(itertools.product(set(df.AP), set(df.DOW), set(df.AS_FLT_NBR))): # restate to pull from combinations of AP / DOW / FLT_NBR / OD
	# prepare training set
	ap = i[0]
	day = i[1]
	nbr = i[2]
	original = df[(df.DOW==day) & (df.AP == ap) & (df.AS_FLT_NBR == nbr)]
	od = pd.unique(original.OD)

	print(f'length of {day}, {ap}AP dataframe: {len(original)}')
	# tmp = original.dropna(axis=1, thresh=len(original)*.67)
	# tmp = original.dropna(axis=1, how='all')
	# tmp = tmp.dropna(axis=0, thresh=len(original.columns)*.25)
	# tmp = tmp.dropna(axis=0, how='any')
	print(f'number of null values: {original.isnull().sum().sum()}')
	tmp = original.fillna(value = 0)
	print(f'length of cleansed {day} dataframe: {len(tmp)}')

	# prepare sample population
	sample=tmp.sample(frac=.2)
	sample_input=sample.filter(regex='\w{2}\d{2}')
	sample_input['DAY_LOWEST'] = tmp['DAY_LOWEST']
	sample_actual=sample.SALE_BKGS
	sample['IS_SAMPLE'] = 1

	# drop samples from training set
	tmp = tmp.merge(sample[['OBSERVATION_DT', 'OD', 'DPTR_DATE', 'AS_FLT_NBR', 'IS_SAMPLE']], how='left', on=['OBSERVATION_DT', 'OD', 'DPTR_DATE', 'AS_FLT_NBR'])
	tmp = tmp[tmp['IS_SAMPLE'] != 1]
	x = tmp.filter(regex='\w{2}\d{2}')
	x['DAY_LOWEST'] = tmp['DAY_LOWEST']
	y = tmp.SALE_BKGS

	# create model
	regr_elasticnet = ElasticNet(random_state=0, selection='random',max_iter=10000000, l1_ratio=.5)
	regr_elasticnet.fit(x, y)

    # store results
	results = pd.DataFrame(columns=col)
	results['COEF'] = pd.DataFrame([regr_elasticnet.coef_], columns=x.columns).to_dict('records')
	results['DOW'] = day
	results['AP'] = ap
	results['FLT_NBR'] = nbr
	results['OD'] = od
	results['NDOD'] = 'LAXJFK'

	# measure R^2
	results['ELASTICNET_SCORE'] = regr_elasticnet.score(x, y)
	print(regr_elasticnet.score(x, y))

	# measure MAE / MSE 
	predict = regr_elasticnet.predict(sample_input)
	predict = [round(item) for item in list(predict)]
	mse = metrics.mean_squared_error(sample_actual,predict)
	mae = metrics.mean_absolute_error(sample_actual, predict)
	results['MSE'] = mse
	results['MAE'] = mae
	print(f'MSE: {mse}')
	print(f'MAE: {mae}')

	# save off results
	# results.drop('placeholder', inplace=True)
	output= output.append(results)

output = output.dropna(axis=1,how='all')
output.to_csv('results.csv')
collection.insert_many(output.to_dict('records'))

