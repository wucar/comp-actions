import pandas as pd
import cx_Oracle
import config
import logging
import math

logger = logging.basicConfig(level='INFO')

# author: caroline wu
# version: 1.0.0
# status: development

#TO DO - BUILD BETTER ERROR HANDLING PLEASE

def disconnectDB(conn):
    """
    Closes connection to DB.

    :param conn: connection object (asoracle)

    """
    conn.close()

# connect to DB, returns connnection
def connectDB(user, password, connectionString):
    """
    Returns a connection object (cx_oracle).

    :param user: username for database
    :param password: password for database
    :param connectionString: oracle connection string
    :return:
    """
    try:
        return cx_Oracle.connect(user, password, connectionString)
    except cx_Oracle.DatabaseError as e:
        error, = e.args
        logger.error('Error code: ', error.code)
        logger.error('Error message: ', error.message)
        logger.error('Error offset: ', error.offset)
        raise

class InputError(Exception):
    """Base class for exceptions in this module."""
    pass

class asoracle(object):

    def __init__(self, user, password, connectionString):
        """
        One asoracle object should be created for each database.

        :param user: username
        :param password: password
        :param connectionString: connection string
        """
        self.user = user
        self.password = password
        self.connStr = connectionString

    def sqltoDF(self, path=None,sql=None):
        """
        If path is populated, then a .sql script will be executed.
        If sql is populated, then the string sql script will be executed.
        Includes error handlign to ensure that only path or sql is populated.

        :param path: file path
        :param sql: sql (string)
        :return: dataframe
        """
        conn = connectDB(self.user, self.password, self.connStr)

        if path is not None and sql is not None:
            raise InputError('sqltoDF: both path and sql cannot be populated')
        elif path is None and sql is None:
            raise InputError('sqltoDF: either path or sql must be populated')
        elif path is not None:
            fd = open(path, 'r')
            sqlFile = fd.read()
            fd.close()
            file_sql = sqlFile.replace('\n', ' ').split(';')[0]
            df = pd.read_sql_query(file_sql, conn)
            # disconnectDB(conn)
            return df
        elif sql is not None:
            df = pd.read_sql_query(sql, conn)
            # disconnectDB(conn)
            return df

    def executeSQL(self, sql):
        """

        Executes sql statement & commits; returns cursor.
        Will rollback sql statement in case of failure.

        :param sql: sql statement (string)
        :return: cursor object (cx_oracle)
        """
        conn = connectDB(self.user, self.password, self.connStr)
        cursor = conn.cursor()

        try:
            cursor.execute(sql)
            conn.commit()
            logger.debug('SQL executed: {}'.format(cursor.statement))
            return cursor
        except cx_Oracle.DatabaseError as e:
            logger.debug('SQL executed: {}'.format(cursor.statement))
            error, = e.args
            logger.error('Error code: ', error.code)
            logger.error('Error message: ', error.message)
            logger.error('Error offset: ', error.offset)
            logger.error('SQL statement: ', sql)
            conn.rollback()
            raise

    def drop_table(self, schema, table_name):
        """
        Drops table.

        :param schema: schema
        :param table_name:  table name

        """
        sql = "DROP TABLE {}.{}".format(schema, table_name)
        self.executeSQL(sql)

    def create_table(self, schema, table_name, params=None, df=None):
        """

        Creates table from either a dict with data types or dataframe.

        :param params: dict (key = column, value = data type, conditions)
         - TAKES PRECEDENCE OVER DF
        :param df: dataframe, will infer data types
        :return:
        """

        if self.tableview_exists(schema, table_name):
            self.drop_table(schema, table_name)

        sql = 'CREATE TABLE {}.{} ('.format(schema, table_name)

        if params is not None:
            for key in params:
                sql = '{} {} {}, '.format(sql, key, params.get(key))
            sql = sql[:-2]+')'
            self.executeSQL(sql)

            grant_sql = f'GRANT SELECT ON {schema}.{table_name} TO {config.oracle_select}'
            self.executeSQL(grant_sql)
            grant_sql = 'GRANT DELETE, INSERT, UPDATE ON {}.{} TO {}'.format(
                schema, table_name, config.oracle_admin
            )
            self.executeSQL(grant_sql)

        elif df is not None:
            d = {}

            for column in df:
                dt = self.infer_datatypes(df[column])
                d[column] = dt

            self.create_table(schema, table_name, params=d)

    def truncate_table(self, schema, table_name):
        """
        Truncates oracle table.

        :param schema: schema
        :param table_name: table_name

        """
        self.delete_records(schema, table_name)

    def infer_datatypes(self, series): # REQUIRES MORE RIGOROUS TESTING
        """
        Takes in series (column in a dataframe) as an argument
        and infers data type and precision.

        Will raise exception if the pandas data type is not recognized.

        :param series:
        :return:
        """

        dtype = series.dtypes

        if dtype.name.find('int') > -1: #tested for both int32 and int64
            return 'NUMBER'
        elif dtype == 'float64':
            return 'FLOAT'
        elif dtype == 'datetime64[ns]': #tested
            return 'TIMESTAMP'
        elif dtype == 'object': #tested
            len = series.str.len().max()
            if math.isnan(len):
                return 'VARCHAR2(30)'
            else:
                return 'VARCHAR2({})'.format(int(len))
        else:
            str = 'asoracle function inferDataTypes ' \
                  'is unable to recognize data type: {}'.format(dtype)
            logger.error(str)
            raise TypeError(str)

    def update_records(self, schema, table_name, set_cond, where_cond=None):
        """
        Sets up and executes SQL update statement.

        :param schema: str
        :param table_name: str
        :param set_cond: {COLUMN: [COND (ex. IS NOT NULL, <=),
        VALUE (does not exist if 'IS NOT NULL']}
        :param where_cond: {COLUMN: [COND (ex. IS NOT NULL, <=),
         VALUE (does not exist if 'IS NOT NULL']}

        """

        sql = 'UPDATE {}.{} SET {} {}'\
            .format(schema, table_name, self.set(set_cond),
                    self.where_clause(where_cond))
        self.executeSQL(sql)

    def update_records_raw(self, schema, table_name, set_cond, where_cond):
        """
        Sets up and executes SQL update statement from raw where clause

        :param schema: str
        :param table_name: str
        :param set_cond: {COLUMN: [COND (ex. IS NOT NULL, <=),
        VALUE (does not exist if 'IS NOT NULL']}
        :param where_cond: raw sql string

        """

        sql = 'UPDATE {}.{} SET {} {}'\
            .format(schema, table_name, self.set(set_cond),
                    where_cond)

        self.executeSQL(sql)

    def delete_records(self, schema, table_name, where_cond=None):
        """
        Sets up and executes SQL delete statement.

        :param schema: str
        :param table_name: str
        :param where_cond: {COLUMN: [COND (ex. IS NOT NULL, <=),
        VALUE (does not exist if 'IS NOT NULL']}
        :return:
        """
        logger.info('Deleting records from {}.{}'.format(schema, table_name))

        start_count = self.count(schema, table_name)

        if where_cond is not None:
            sql = 'DELETE FROM {}.{} {}'.format(schema, table_name,
                                                self.where_clause(where_cond))
        else:
            sql = 'DELETE FROM {}.{}'.format(schema, table_name)

        self.executeSQL(sql)

        end_count = self.count(schema, table_name)

        logger.debug('Number of records deleted: {}'.format(start_count - end_count))

    def insert_records(self, schema, table_name, df):
        """

        :param df: records to be appended to table_name
        :param table_name: table_name in db

        """
        string = ''

        conn = connectDB(self.user, self.password, self.connStr)
        cursor = conn.cursor()

        if self.tableview_exists(schema, table_name) == False:
            logger.warning('Cannot insert records, schema / table '
                           'does not exist: {}.{}'
                           .format(schema, table_name))
            logger.warning('Creating table {}.{}'.format(schema, table_name))
            self.create_table(schema, table_name, df = df)

        df = df.where((pd.notnull(df)), None)

        col = ', '.join(df.columns)

        for i in range(1, len(df.columns) + 1):
            if i < len(df.columns):
                string = string + ':' + str(i) + ','
            else:
                string = string + ':' + str(i)

        cursor.prepare('INSERT INTO {schema}.{table} ({col}) VALUES ({vals})' \
                       .format(schema = schema,
                               table = table_name,
                               col = col,
                               vals = string))

        vals = df.values.tolist()

        try:
            cursor.executemany(None, vals)
            conn.commit()
            logger.debug('SQL executed: {}'.format(cursor.statement))
        except cx_Oracle.DatabaseError as e:
            logger.error('SQL executed: {}'.format(cursor.statement))
            error, = e.args
            logger.error('Error code: ', error.code)
            logger.error('Error message: ', error.message)
            logger.error('Error offset: ', error.offset)
            conn.rollback()
            raise

    def tableview_exists(self, schema, table_name):
        """
        Verifies if the table / view exists in the database.

        :param schema:
        :param table_name:
        :return:
        """
        sql = "SELECT COUNT(*) FROM ALL_OBJECTS WHERE OBJECT_TYPE IN " + \
              "('TABLE', 'VIEW') AND OBJECT_NAME = '" + \
              str.upper(table_name) + "' AND OWNER = '" + \
              str.upper(schema) + "'"

        cursor = self.executeSQL(sql)
        val = cursor.fetchone()

        if val[0] == 0:
            return False
        elif val[0] > 0:
            return True
        else: # need to try to figure out how to hit this...
            logger.error('tableview_exists: Value is not valid')
            raise InputError('tableview_exists: Value is not valid')

    def check_date_exists(self, schema, table_name, snap=None, load=None):
        """

        :param schema: str
        :param table_name: str
        :param snapshot: value of snap
        :param load: true if load date exists on table and should be
        validated against today's date
        :return: boolean
        """

        maxDate = True

        count = self.count(schema, table_name)

        # if table is not empty
        if count > 0:
            # if no criteria has been set, throw error
            if load is None and snap is None:
                logger.error('check_date_exists: parameters snapshot '
                             'and load are both None')
                raise Exception('check_date_exists: parameters snapshot '
                                'and load are both None')
            else:
                if load is not None:
                    table_load = self.count(schema, table_name,
                                            where_cond = {'LOAD_DATE': load})
                    logger.debug('Count {} load date {}: {}'
                                .format(table_name, str(load),
                                        str(table_load)))
                    maxDate = maxDate and (0 != table_load)

                if snap is not None:
                    table_snap = self.count(schema, table_name,
                                            where_cond={'SNAPSHOT_DATE': snap})
                    logger.debug('Count {} snapshot date {}: {}'
                                .format(table_name, str(snap),
                                        str(table_snap)))
                    maxDate = maxDate and (0 != table_snap)
        else: # if table is empty
            maxDate = False

        return maxDate

    def max_value(self, schema, table_name, column):
        """
        Returns max value of column in specified table.

        :param schema:
        :param table_name:
        :param column:
        :return:
        """

        sql = 'SELECT MAX({}) FROM {}.{}'.format(column, schema,
                                                 table_name)
        cursor = self.executeSQL(sql)
        return cursor.fetchone()[0]

    def count(self, schema, table_name, where_cond=None):
        """
        Return row count of table.

        :param schema:
        :param table_name:
        :param where_cond:
        :return:
        """

        if where_cond is not None:
            sql = 'SELECT COUNT(*) FROM {}.{} {}'\
                .format(schema, table_name, self.where_clause(where_cond))
        else:
            sql = 'SELECT COUNT(*) FROM {}.{}'\
                .format(schema, table_name)

        cursor = self.executeSQL(sql)
        return cursor.fetchone()[0]

    def return_all(self, schema, table_name, snap=None):
        """
        Returns all data in a table (SELECT *)

        :param schema:
        :param table_name:
        :return:
        """
        if snap is None:
            sql = 'SELECT * FROM {}.{}'.format(schema, table_name)
        else:
            d = {'SNAPSHOT_DATE': snap}
            sql = 'SELECT * FROM {}.{} {}'.format(schema, table_name,
                                                   self.where_clause(d))
        return self.sqltoDF(sql=sql)

    def where_clause(self, where_cond):
        sql = ' WHERE '
        for key in where_cond:
            col = key
            cond = where_cond.get(key)
            if 'list' in str(type(cond)):
                if len(cond) == 1:
                    sql = '{} {} {} AND'.format(sql, col, cond[0])
                else:
                    sql = '{} {} {} {} AND'\
                        .format(sql, col, cond[0],
                                self.sql_type_handling(cond[1]))
            else:
                sql = "{} {} = {} AND"\
                    .format(sql, col, self.sql_type_handling(cond))

        sql = sql[:-4]

        return sql

    def sql_type_handling(self, val):
        if 'datetime' in str(type(val)):
            return f"'{val.strftime('%d%b%y')}'"
        elif 'str' in str(type(val)) and val != 'NULL':
            return f"'{val}'"
        else:
            return val

    def set(self, set_cond):
        sql = ''

        for key in set_cond:
            col = key
            cond = set_cond.get(key)
            sql = "{} {} = {} ,"\
                .format(sql, col, self.sql_type_handling(cond))

        sql = sql[:-1]

        return sql

