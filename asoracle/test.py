from asoracle import asoracle as ora
from asoracle import secrets
import pandas as pd
from datetime import date, datetime

def test1():
    print("connected")
    str = "select distinct NDOD from abk_dw.market_dim"
    a = edw.executeSQL(str)
    print("disconnected")

def test2():
    print("connected")
    df = edw.sqltoDF('test.sql')
    print(df)
    print("disconnected")

def test_insertrecords():
    df = pd.DataFrame([[1,2,3], [4,5,6], [7,8,9]], columns=['t1', 't2', 't3'])
    edw.insert_records('CWU', 'PYTEST', df)

def test_droptable():
    schema = 'cwu'
    table_name = 'customers'
    edw.drop_table(schema, table_name)

def test_createtableparams():
    schema = 'cwu'
    table_name = 'customers'
    params = {'customer_id': 'number(10) NOT NULL',
            'customer_name': 'varchar2(50) NOT NULL',
            'city': 'varchar2(50)'}
    edw.create_table(schema, table_name, params)

def test_createtabledf():
    schema = 'cwu'
    table_name = 'PYTEST'
    df = pd.DataFrame([[1, 2, 3], [4, 5, 6], [7, 8, 9]], columns=['t1', 't2', 't3'])
    edw.create_table(schema, table_name, df=df)

def test_createtabledf2():
    schema = 'cwu'
    table_name = 'PYTEST2'
    df = pd.DataFrame([[2, 3], [5, 6], [8, 9]], columns=['t2', 't3'])
    df['t1'] = pd.to_datetime(date.today())
    print(df)
    edw.create_table(schema, table_name, df=df)
    edw.insert_records(schema, table_name, df)

def test_updatetable():
    schema = 'cwu'
    table_name = 'PYTEST'
    set_params = {'t1': 5000}
    where_params = {'t1': ['=', 1], 't2': ['IS NOT NULL']}
    edw.update_records(schema, table_name, set_params, where_params)

def test_updatetablenowhere():
    schema = 'cwu'
    table_name = 'PYTEST'
    set_params = {'t1': 5000}
    edw.update_records(schema, table_name, set_params)

def test_deleterecords():
    schema = 'cwu'
    table_name = 'PYTEST'
    where_params = {'t1': ['=', 5000]}
    edw.delete_records(schema, table_name, where_params)

def test_truncatetable():
    schema = 'cwu'
    table_name = 'PYTEST'
    edw.truncate_table(schema, table_name)

def test_tablexists():
    schema = 'cwu'
    table_name = 'PYTEST'
    edw.tableview_exists(schema, table_name)

def test_maxdate():
    schema = 'AMATSON'
    table_name = 'HALCYON_SL'
    edw.check_max_date(schema, table_name)

def test_maxdate_missingparams():
    schema = 'AMATSON'
    table_name = 'HALCYON_SL'
    edw.check_max_date(schema, table_name, snapshot=date.today())

def testend2end_v1():
    schema = 'CWU'
    table_name = 'PYTEST3'
    df = pd.DataFrame([[date.today(), 2, 3], [date.today(), 5, 6], [date.today(), 8, 9]], columns=['t1', 't2', 't3'])
    df['t1'] = pd.to_datetime(df['t1'])
    print(df)
    print(df.dtypes)
    edw.create_table(schema, table_name, df=df)
    edw.insert_records(schema, table_name, df)

def test_count():
    schema = 'cwu'
    table_name = 'PYTEST'
    where_params = {'T1': [1]}
    print(edw.count(schema, table_name, where_params))

print("hello world")
# cred = ora.inputCredentials()
edw = ora.asoracle(secrets.user,secrets.password,"DATAWAREHOUSE.DB.INSIDEAAG.COM:1522/DATAWHSE")
# test1()
# test2()
# test_insertrecords()
# test_createtableparams()
test_createtabledf()
# test_createtabledf2()
# test_droptable()
# test_updatetable()
# test_updatetablenowhere()
# test_deleterecords()
# test_truncatetable()
# # test_tablexists()
# test_maxdate()
# test_maxdate_missingparams()
# testend2end_v1()
# test_count()